from django.apps import AppConfig


class FancyappsConfig(AppConfig):
    name = 'fancyapps'
